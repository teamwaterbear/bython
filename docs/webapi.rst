.. Bython documentation master file, created by
   sphinx-quickstart on Fri Oct 27 18:57:13 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Web API
==================================
.. toctree::
   :maxdepth: 2
   :caption: Contents:
   
The Web API, unlike the Python API, relies upon the RESTful way of doing things. All commands will be access via URL and data being sent through either a GET, POST, or other actions that can be done through the web.
   
How it works
-----------------------
An application that uses the RESTful method will connect to the URL that fulfulls the method, such as:

.. code-block:: html

	http://yourdomainname.com/Authenticate
	
Which will receive data via a post action, which will use JSON objects for the data, or get action. There will then be a JSON response from the server providing the proper information that resulted from running that command.

Note: Not all Web API functions have been implemented yet.

Web API Functions
-----------------------
===================================  ==================  ======  ============================
Name                                 JSON Args           Type    Description
===================================  ==================  ======  ============================
Authenticate                         username, password  POST    Sign into Booker account
Schedules                            N/A                 GET     Gets all schedules
Schedules/<sID>                      N/A                 GET     Gets schedule with sID
Schedules/<sID>/Slots                N/A                 GET     Get slots of schedule with sID
Users                                N/A                 GET     Get users visible to current user AUTH
Users/<uID>                          N/A                 GET     Get user with uID
Reservations                         N/A                 GET     Get all reservations, or specify startDate and endDate in url
Reservations/<refNum>                N/A                 GET     Get rservation of refNum
Resources                            N/A                 GET     Get all resources
Resources/Availability               N/A                 GET     Get available resources
Resources/<resourceId>               N/A                 GET     Get resource by resourceId
Resources/<resourceId>/Availability  N/A                 GET     Get resource available by ID
Resources/Status                      N/A                 GET     Get resource statuses
===================================  ==================  ======  ============================