.. Bython documentation master file, created by
   sphinx-quickstart on Fri Oct 27 18:57:13 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Python API
==================================
.. toctree::
   :maxdepth: 2
   :caption: Contents:
   
Basic Structure
-----------------------
There is a structure involved within the library, which is to authenticate, get/post, and then receive a response. The following codew shows this process.

.. code-block:: python
   :linenos:

	bconn = bython.Booker('[IPADDRESS]', 10)
	print(bconn.AUTH('username', 'password')["success"])
	print(bconn.getSlots(1))
	print(bconn.EXTRACT_JSON())
	bconn.signOut()
	bconn.CLOSE()
	
All interactions with the Booker server will involve this structure of code.

For GET calls, the following will be the structure of the code:

.. code-block:: python
   :linenos:

	bconn = bython.Booker('[IPADDRESS]', 10)
	print(bconn.AUTH('username', 'password')["success"]) # AUTHENTICATION
	print(bconn.getSlots(1))	# GET COMMAND
	print(bconn.EXTRACT_JSON()) # RESPONSE
	bconn.signOut()
	bconn.CLOSE()
	
There are three choices for responses, which are to get the raw bytes of the response, the string of the response, or the json that corresponds with the response.

For POST calls, the following will be the structure of the code:

.. code-block:: python
   :linenos:
   
   reservation = {"description":"reservation description",
               "endDateTime":"2017-10-20T10:30:00-0700",
               "resourceId":1,
               "resources":[1],
               "startDateTime":"2017-10-20T10:00:00-0700",
               "title":"reservation title",
               "userId":3,
               "startReminder":{"value":15,"interval":"days"},
               "endReminder":None,
               "allowParticipation":False}

	bconn = bython.Booker('[IPADDRESS]', 10)
	print(bconn.AUTH('username', 'password')["success"])
	bconn.SET_DATA(reservation)
	print(bconn.createReservation())
	bconn.signOut()
	bconn.CLOSE()
	
First, the data to be sent must be initialized as a dictionary in python. Then, the SET_DATA function, which takes the dictionary variable, sets the data to be posted. After the data has been set, a POST command can be called to send the data. 

Booker API Functions
-----------------------
====================  ====  ======  ====
Name                  Args   Type    Description
====================  ====  ======  ====
createReservation     N/A   POST    Gets data from last SET_DATA call, creates a reservation
updateReservation     refN  POST    Update a reservation that has reference refN(string)
getReservations       N/A   GET     Gets all reservations for next two weeks
getReservation        refN  GET     Gets reservation that has reference refN(string)
checkInReservation    refN  POST    Checks in a reservation
checkOutReservation   refN  POST    Checks out a reservation
deleteReservation     refN  DEL     Deletes reservation with reference number refN(string)
getAllSchedules       N/A   GET     Gets all schedules
getSchedule           sId   GET     Gets schedule with sId(int)
getSlots              sId   GET     Gets slots of schedule with sId(int)
getUser               uId   GET     Get user information by uId
getAllUsers           N/A   GET     Get all users that AUTH account can see
getResourceStatuses   N/A   GET     Get status of resources
getAllResources       N/A   GET     Get all resources
getResourceAvail      N/A   GET     Get all resource availability
getResourceAvailById  rId   GET     Get resource availability by rId(int)
getResource           rId   GET     Get information on resource with rId(int)
signOut               N/A   POST    Sign out of current user. Does not require SET_DATA
====================  ====  ======  ====

Primitive API Functions
-----------------------
=============  =========  ===============================================
Name           Args       Description
=============  =========  ===============================================
AUTH           user,pass  Log in to Booker account, stores info in object
POST           url_cmd    Send data from SET_DATA post request to url_cmd 
GET            url_cmd    Get data from url_cmd
DELETE         url_cmd    Delete data on server from url_cmd
SET_DATA       data       Sets json data in Booker object to be sent
SET_HEADER     data       Sets json header in Booker object to be sent
CLOSE          N/A        Closes the connection
RESPONSE_BYTE  N/A        Returns json response(in bytes) from last call
RESPONSE_STR   N/A        Returns json response(as a str) from last call
EXTRACT_JSON   N/A        Returns json response(as json) from last call
=============  =========  ===============================================