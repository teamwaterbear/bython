.. Bython documentation master file, created by
   sphinx-quickstart on Fri Oct 27 18:57:13 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Bython
==================================
  
Welcome to the Bython python library documentation! This documentation provides all the commands provided by the Bython library to access features of the Booker API. 

Python API Reference
====================
Here are the specific commands and Booker calls that can be made from Bython.

.. toctree::
   :maxdepth: 2
   
   pythonapi

Web API Reference
==================
There is also a Flask component that can be used to make the calls to the Booker API. The commands will be in the form of [IPADDRESS]/[Command]. All data to be sent to the Flask component will be done through POST actions.

.. toctree::
   :maxdepth: 2
   
   webapi
